package com.parham.graph.controller;

import com.parham.graph.Exception.NotFoundException;
import com.parham.graph.Exception.ValidationException;
import com.parham.graph.domain.Graph;
import com.parham.graph.domain.CalculateRequest;
import com.parham.graph.service.MyService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
public class MyController {
    private final MyService service;

    @PostMapping(path = "/Topology", consumes = "application/json")
    public ResponseEntity<String> saveGraph(@RequestBody Graph graph) {
        try {
            service.saveGraph(graph);
            return new ResponseEntity<>("Graph successfully added", HttpStatus.OK);
        } catch (ValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping(path = "/calculate-path", consumes = "application/json")
    public ResponseEntity<String> calculateNearestPath(@RequestBody CalculateRequest request) {
        try {
            String nearestPath = service.calculateNearestPath(request);
            return new ResponseEntity<>(nearestPath, HttpStatus.OK);
        } catch (NotFoundException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        } catch (ValidationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
}
