package com.parham.graph.service.validation;

import com.parham.graph.Exception.ValidationException;
import com.parham.graph.domain.Graph;
import com.parham.graph.domain.CalculateRequest;
import org.springframework.stereotype.Component;

@Component
public class MyValidation {

    public void isValidRequest(CalculateRequest requestDto) throws ValidationException {
        if (requestDto.getPathDestination() == null || requestDto.getPathSource() == null)
            throw new ValidationException("Not valid request");
    }

    public void isValidGraph(Graph graph) throws ValidationException {
        if (graph.getNode() == null || graph.getEdges() == null || graph.getNode().size() == 0
                || graph.getEdges().size() == 0)
            throw new ValidationException("Not valid graph");
    }
}
