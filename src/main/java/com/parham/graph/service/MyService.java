package com.parham.graph.service;

import com.parham.graph.Exception.NotFoundException;
import com.parham.graph.Exception.ValidationException;
import com.parham.graph.domain.Edge;
import com.parham.graph.domain.Graph;
import com.parham.graph.domain.CalculateRequest;
import com.parham.graph.service.validation.MyValidation;
import lombok.RequiredArgsConstructor;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@RequiredArgsConstructor(onConstructor_ = @Autowired)
@Service
public class MyService {
    private final MyValidation validation;
    private final MongoTemplate template;

    private final Long id = 1L;

    public void saveGraph(Graph graph) throws ValidationException {
        graph.setId(id);
        validation.isValidGraph(graph);
        template.save(graph);
    }

    public String calculateNearestPath(CalculateRequest requestDto) throws NotFoundException, ValidationException {
        validation.isValidRequest(requestDto);
        DirectedGraph<String, DefaultEdge> directedGraph = getDirectedGraph(getGraphById(id));
        DijkstraShortestPath<String, DefaultEdge> dijkstraShortestPath = new DijkstraShortestPath<>(directedGraph);
        try {
            List<String> shortestPath = dijkstraShortestPath
                    .getPath(requestDto.getPathSource(), requestDto.getPathDestination())
                    .getVertexList();
            return shortestPath.toString();
        } catch (NullPointerException e) {
            throw new NotFoundException("There is no path");
        }
    }

    private Graph getGraphById(Long id) throws NotFoundException {
        Graph graph = template.findById(id, Graph.class);
        if (graph != null)
            return graph;
        throw new NotFoundException("Can not find this graph");
    }

    private DirectedGraph<String, DefaultEdge> getDirectedGraph(Graph graph) {
        DirectedGraph<String, DefaultEdge> directedGraph = new DefaultDirectedGraph<>(DefaultEdge.class);
        for (int i : graph.getNode())
            directedGraph.addVertex(String.valueOf(i));

        for (Edge edge : graph.getEdges())
            directedGraph.addEdge(edge.getSource(), edge.getDestination());
        return directedGraph;
    }
}
