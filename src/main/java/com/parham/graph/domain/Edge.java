package com.parham.graph.domain;

import lombok.Data;

@Data
public class Edge {
    private String source;
    private String destination;
}
