package com.parham.graph.domain;

import lombok.Data;

@Data
public class CalculateRequest {
    private String pathSource;
    private String pathDestination;
}
