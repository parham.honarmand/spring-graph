package com.parham.graph.domain;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.annotation.Id;

import java.util.List;

@Data
@Document(collation = "graph")
public class Graph {
    @Id
    private Long id;
    private List<Integer> node;
    private List<Edge> edges;
}
