package com.parham.graph.Exception;

public class ValidationException extends Exception {
    public ValidationException(String message) {
        super(message);
    }
}
